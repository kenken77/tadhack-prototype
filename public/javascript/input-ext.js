/**
 * Created by Kenneth on 17/9/2016.
 */
const HTTPS_PROTOCOL = 'http://';
//const HTTP_PROTOCOL = 'http://';

function dance(){
    turnLeft();
    setTimeout(function(){
    }, 3000);
    turnRight();
    setTimeout(function(){
    }, 3000);
    stopMotor();
    setTimeout(function(){
    }, 2000);
    turnLeft();
    setTimeout(function(){
    }, 3000);
    turnRight();
    setTimeout(function(){
    }, 3000);
    stopMotor();
}

function turnLeft(){
    $.ajax({
        type: 'GET',
        url: HTTPS_PROTOCOL+ window.location.host + '/turn-left',
        success: function(data) {
        }
    });
}

function stopMotor(){
    $.ajax({
        type: 'GET',
        url: HTTPS_PROTOCOL+ window.location.host + '/stop-motor',
        success: function(data) {
        }
    });
}

function turnRight(){
    $.ajax({
        type: 'GET',
        url: HTTPS_PROTOCOL+ window.location.host + '/turn-right',
        success: function(data) {
        }
    });
}

function goStraight(){
    $.ajax({
        type: 'GET',
        url: HTTPS_PROTOCOL+ window.location.host + '/move-forward',
        success: function(data) {
        }
    });
}

function goBackward(){
    $.ajax({
        type: 'GET',
        url: HTTPS_PROTOCOL+ window.location.host + '/move-backward',
        success: function(data) {
        }
    });
}

$("#test").swipe( {
    //Generic swipe handler for all directions
    swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
        if(direction=='up'){
            goStraight();
        }else if(direction='down'){
            goBackward();
        }else if(direction='right'){
            turnRight();
        }else if(direction='left'){
            turnLeft();
        }
        stopMotor();
    }
});

$(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
            console.log("turn left");
            turnLeft();
            break;

        case 38: // up
            console.log("Straight");
            goStraight();
            break;

        case 39: // right
            console.log("turn Right");
            turnRight();
            break;

        case 40: // down
            console.log("backward");
            goBackward();
            break;

        case 27: // STOP
            console.log("STOP");
            stopMotor();
            break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});

$(document).keyup(function(e) {
    stopMotor();
});
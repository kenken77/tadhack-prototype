var BBCMicrobit = require('bbc-microbit');
var path = require("path");
var express = require("express");
var redis = require('redis');
client = redis.createClient(6379, '128.199.169.78');

var app = express();

var WatchJS = require("watchjs")
var watch = WatchJS.watch;
var async = require('async');
var sleep = require('sleep');

var bodyParser = require("body-parser");
var session = require("express-session");
var cookieParser = require('cookie-parser');

var moment = require('moment-timezone');
const SERVER_TZ = "Asia/Singapore";

const  PORT = "port";
var period = 160; // ms
var microbitTemperature = 0;

var buttonAState = {};
var buttonBState = {};

var pin8Value = 1;
var pin12Value =  1;
var pin0Value =  1;
var pin16Value = 1;

var directionChanges = {
    direction: 'S'
}

const PIN8 = 8;
const PIN12 = 12;
const PIN0 = 0;
const PIN16 = 16;


app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Initialize session
app.use(session({
    secret: "nus-stackup-iot",
    resave: false,
    saveUninitialized: true
}));

var BUTTON_VALUE_MAPPER = ['Not Pressed', 'Pressed', 'Long Press'];

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


client.on("error", function (err) {
    console.log("Error " + err);
});

console.log('Scanning for microbit');
BBCMicrobit.discover(function(microbit) {
    console.log('\tdiscovered microbit: id = %s, address = %s', microbit.id, microbit.address);

    console.log('connecting to microbit');
    microbit.connectAndSetUp(function () {
        console.log('\tconnected to microbit');

        microbit.subscribeButtons(function () {
            console.log('\tsubscribed to buttons');
            sleep.usleep(1000000);
        });

        microbit.writeMagnetometerPeriod(period, function () {
            console.log('\tmagnetometer period set');

            console.log('subscribing to magnetometer');
            microbit.subscribeMagnetometer(function () {
                console.log('\tsubscribed to magnetometer');
            });
        });

        microbit.writeTemperaturePeriod(period, function () {
            console.log('\ttemperature period set');

            console.log('subscribing to temperature');
            microbit.subscribeTemperature(function () {
                console.log('\tsubscribed to temperature');
            });
        });

        console.log('setting accelerometer period to %d ms', period);
        microbit.writeAccelerometerPeriod(period, function () {
            console.log('\taccelerometer period set');

            console.log('subscribing to accelerometer');
            microbit.subscribeAccelerometer(function () {
                console.log('\tsubscribed to accelerometer');
            });

        });

        microbit.pinOutput(PIN8, function () {
            console.log('\tpin set as output');

            console.log('setting pin %d as digital', 8);
            microbit.pinDigital(PIN8, function () {
                console.log('\tpin set as digital ' + pin8Value);
            });
        });

        microbit.pinOutput(PIN12, function () {
            console.log('\tpin set as output');

            console.log('setting pin %d as digital', 12);
            microbit.pinDigital(PIN12, function () {
                console.log('\tpin set as digital ' + pin12Value);
            });
        });

        watch(directionChanges, ["direction"], function () {
            console.log(directionChanges.direction);
            async.parallel({
                    motorone: function (callback) {
                        setTimeout(function () {
                            togglePin(PIN0, parseInt(pin0Value));
                            togglePin(PIN16, parseInt(pin16Value));
                            callback(null, 1);
                        }, 200);
                    },
                    motortwo: function (callback) {
                        setTimeout(function () {
                            togglePin(PIN8, parseInt(pin8Value));
                            togglePin(PIN12, parseInt(pin12Value));
                            callback(null, 2);
                        }, 100);
                    }
                },
                function (err, results) {
                    console.log(results);
                });

        });

        microbit.pinOutput(PIN0, function () {
            console.log('\tpin set as output');

            console.log('setting pin %d as digital', 0);
            microbit.pinDigital(PIN0, function () {
                console.log('\tpin set as digital ' + pin0Value);
            });
        });

        microbit.pinOutput(PIN16, function () {
            console.log('\tpin set as output');

            console.log('setting pin %d as digital', 16);
            microbit.pinDigital(PIN16, function () {
                console.log('\tpin set as digital ' + pin16Value);
            });
        });

        function togglePin(pin, pinValue) {
            console.log('writing %d to pin %d', pinValue, pin);
            microbit.writePin(pin, pinValue, function () {
                console.log('\tdone');

            });
        }
    });

    microbit.on('disconnect', function () {
        console.log('\tmicrobit disconnected!');
    });

    microbit.on('accelerometerChange', function (x, y, z) {
        //console.log('\ton -> accelerometer change: accelerometer = %d %d %d G', x.toFixed(1), y.toFixed(1), z.toFixed(1));
        var accelerometerData = {};
        accelerometerData = {
            sensorType: 'accelerometer',
            accelerometerX: x.toFixed(1),
            accelerometerY: y.toFixed(1),
            accelerometerZ: z.toFixed(1)
        };
        client.hmset("accelerometerData", accelerometerData, function (err, res) {
            accelerometerData = {};
        });
    });

    microbit.on('buttonAChange', function (value) {
        console.log('\ton -> button A change: ', BUTTON_VALUE_MAPPER[value]);
        buttonAState = {sensorType: 'buttonA', buttonAState: BUTTON_VALUE_MAPPER[value]};
        client.hmset("buttonAState", buttonAState, function (err, res) {
            buttonAState = {};
        });
    });

    microbit.on('buttonBChange', function (value) {
        console.log('\ton -> button B change: ', BUTTON_VALUE_MAPPER[value]);
        buttonBState = {sensorType: 'buttonB', buttonBState: BUTTON_VALUE_MAPPER[value]};
        client.hmset("buttonBState", buttonBState, function (err, res) {
            buttonBState = {};
        });
    });

    microbit.on('magnetometerChange', function (x, y, z) {
        var magnetometerData = {};
        //console.log('\ton -> magnetometer change: magnetometer = %d %d %d', x.toFixed(1), y.toFixed(1), z.toFixed(1));
        magnetometerData = {
            sensorType: 'magnetometer',
            magnetometerX: x.toFixed(1),
            magnetometerY: y.toFixed(1),
            magnetometerZ: z.toFixed(1)
        };
        client.hmset("magnetometerData", magnetometerData, function (err, res) {
            magnetometerData = {};
        });
    });

    microbit.on('temperatureChange', function (temperature) {
        //console.log('\ton -> temperature change: temperature = %d °C', temperature);
        microbitTemperature = {sensorType: 'temperature', temperatureValue: temperature};
        ;
        client.hmset("microbitTemperature", microbitTemperature, function (err, res) {
            microbitTemperature = {};
        });
    });

});


/**
 * move forward
 */
app.get('/move-forward', function(request, response) {
    console.log("forward");
    pin0Value = 1;
    pin16Value = 0;
    pin8Value = 1;
    pin12Value = 0;
    directionChanges.direction = 'F';
    response.status(200).json("{status: 'true'}");
});

/**
 * move backward
 */
app.get('/move-backward', function(request, response) {
    console.log("backward");
    pin0Value = 0;
    pin16Value = 1;
    pin8Value = 0;
    pin12Value = 1;
    directionChanges.direction = 'B';
    response.status(200).json("{status: 'true'}");
});

/**
 * stop motor
 */
app.get('/stop-motor', function(request, response) {
    console.log("stop");
    pin0Value = 1;
    pin16Value = 1;
    pin8Value = 1;
    pin12Value = 1;
    directionChanges.direction = 'S';
    response.status(200).json("{status: 'true'}");
});

/**
 * turn right
 */
app.get('/turn-right', function(request, response) {
    console.log("turn right");
    pin0Value = 1;
    pin16Value = 0;
    pin8Value = 0;
    pin12Value = 0;
    directionChanges.direction = 'R';
    response.status(200).json("{status: 'true'}");
});

/**
 * turn left
 */
app.get('/turn-left', function(request, response) {
    console.log("turn left ");
    pin0Value = 0;
    pin16Value = 0;
    pin8Value = 1;
    pin12Value = 0;
    directionChanges.direction = 'L';
    response.status(200).json("{status: 'true'}");
});

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.set(PORT, process.argv[2] || process.env.APP_PORT || 3000);

var server = app.listen(app.get(PORT) , function(){
    console.info("App Server started on " + app.get(PORT));
});

var io = require('socket.io')(server);
var numUsers = 0;

io.on('connection', function (socket) {
    var addedUser = false;
    // when the client emits 'new message', this listens and executes
    socket.on('new message', function (data) {
        // we tell the client to execute 'new message'
        var todayDate = new Date();
        var dateInString = moment(todayDate).tz(SERVER_TZ).format();
        console.log("Memory heap used --> " + process.memoryUsage().heapUsed);
        var jsonStr =  '{"sensor-data":[]}';
        var outGoingMessages = JSON.parse(jsonStr);

        async.series({
                one: function(callback){

                    setTimeout(function(){
                        client.hgetall("accelerometerData", function (error, result) {
                            if(error){
                                console.error(error);
                            }
                            outGoingMessages['sensor-data'].push(result);
                        });
                        client.hgetall("magnetometerData", function (error, result) {
                            if(error){
                                console.error(error);
                            }
                            outGoingMessages['sensor-data'].push(result);
                        });

                        client.hgetall("microbitTemperature", function (error, result) {
                            if(error){
                                console.error(error);
                            }
                            outGoingMessages['sensor-data'].push(result);
                        });
                        client.hgetall("buttonAState", function (error, result) {
                            if(error){
                                console.error(error);
                            }
                            console.log(result);
                            outGoingMessages['sensor-data'].push(result);
                        });

                        client.hgetall("buttonBState", function (error, result) {
                            if(error){
                                console.error(error);
                            }
                            console.log(result);
                            outGoingMessages['sensor-data'].push(result);
                        });

                        client.hgetall("PatientButtonAState", function (error, result) {
                            if(error){
                                console.error(error);
                            }
                            console.log(result);
                            outGoingMessages['sensor-data'].push(result);
                        });

                        client.hgetall("PatientButtonBState", function (error, result) {
                            if(error){
                                console.error(error);
                            }
                            console.log(result);
                            outGoingMessages['sensor-data'].push(result);
                        });
                        callback(null, 1);
                    }, 200);
                },
                two: function(callback){
                    setTimeout(function(){

                        callback(null, 2);
                    }, 200);
                    //callback(null, 2);
                }
            },
            function(err, results) {
                // results is now equals to: {one: 1, two: 2}
                console.log(outGoingMessages['sensor-data'].length);
                console.log(outGoingMessages['sensor-data']);
                if (results.one == 1 && results.two == 2) {
                    console.log(outGoingMessages['sensor-data']);
                    socket.emit('new message', {
                        username: socket.username,
                        message: JSON.stringify(outGoingMessages)
                    });
                    //    break;
                }
            });
    });

    // when the client emits 'add user', this listens and executes
    socket.on('add user', function (username) {
        if (addedUser) return;

        // we store the username in the socket session for this client
        socket.username = username;
        ++numUsers;
        addedUser = true;
        socket.emit('login', {
            numUsers: numUsers
        });
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers
        });
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', function () {
        socket.broadcast.emit('typing', {
            username: socket.username
        });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', function () {
        socket.broadcast.emit('stop typing', {
            username: socket.username
        });
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function () {
        if (addedUser) {
            --numUsers;

            // echo globally that this client has left
            socket.broadcast.emit('user left', {
                username: socket.username,
                numUsers: numUsers
            });
        }
    });
});